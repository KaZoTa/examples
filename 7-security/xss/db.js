import mysql from 'mysql';
import { promisify } from 'util';

const pool = mysql.createPool({
  connectionLimit: 10,
  multipleStatements: true,
  database: 'webprog',
  host: 'localhost',
  port: 3306,
  user: 'webprog',
  password: 'VgJUjBd8',
});

export const query = promisify(pool.query).bind(pool);

export const escape = pool.escape.bind(pool);

query(`
  DROP TABLE IF EXISTS blogPosts;
  DROP TABLE IF EXISTS users;

  CREATE TABLE blogPosts (
    id BIGINT NOT NULL AUTO_INCREMENT,
    title VARCHAR(64),
    content VARCHAR(1024),
    date VARCHAR(50),
    PRIMARY KEY (id)
  );
  CREATE TABLE users (
    id BIGINT NOT NULL AUTO_INCREMENT,
    username VARCHAR(24),
    password VARCHAR(256),
    PRIMARY KEY (id)
  );

  INSERT INTO users VALUES (1, 'User 1', 'Password 1 (not hashed)');
  INSERT INTO users VALUES (2, 'User 2', 'Password 2 (not hashed)');

  INSERT INTO blogPosts VALUES (1, 'Title 1', 'Content 1', '2019-05-29 11:00:00');
  INSERT INTO blogPosts VALUES (2, 'Title 2', 'Content 2', '2019-05-29 12:00:00');
  INSERT INTO blogPosts VALUES (3, 'Title 3', 'Content 3', '2019-05-29 14:00:00');
`);
