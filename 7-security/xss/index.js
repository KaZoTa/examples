/**
 * Példa egy szerveroldali cross site scripting-re érzékeny oldalra.
 */
import express from 'express';
import path from 'path';
import { query, escape } from './db.js';

const app = express();

app.use(express.static(path.join(process.cwd(), 'static')));
app.use(express.urlencoded({ extended: true }));

app.set('view engine', 'ejs');

app.get('/', async (req, res) => {
  const { titleQuery } = req.query;

  let queryString = 'select * from blogPosts';
  if (titleQuery) {
    queryString += ` where title like '%${escape(titleQuery)}%'`;
  }

  // egy süti amit el lehet lopni (mert nem HttpOnly)
  res.cookie('superSecretCookie', 'superSecretCookieValue');

  try {
    const blogPosts = await query(queryString);
    res.render('blogPosts', { blogPosts, titleQuery });
  } catch (err) {
    res.render('blogPosts', { err, titleQuery });
  }
});

app.post('/', (req, res) => {
  if (req.body && req.body.title && req.body.content) {
    const blogPost = {
      title: req.body.title,
      content: req.body.content,
      date: new Date().toISOString(),
    };
    const queryString = 'INSERT INTO blogPosts SET ?';
    query(queryString, blogPost, (err) => {
      if (err) throw err;
      res.redirect('/');
    });
  } else {
    res.redirect('/');
  }
});

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
