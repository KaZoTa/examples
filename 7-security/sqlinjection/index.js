/**
 * Példa egy SQL injection támadásra.
 * Egy kereső form bemenete nincs lekezelve megfelelően.
 * Próbáljuk a következő keresési értékeket a formban:
    1
    1%'-- comment
    1%' union select 1-- comment
    1%' union select 1,2,3,4-- comment
    1%' union select 1,database(),3,4 from dual-- comment
    1%' union select 1,table_name,3,4 from information_schema.tables
      where table_schema='webprog'-- comment
    1%' union select 1,column_name,3,4 from information_schema.columns
      where table_name='users' and table_schema='webprog'-- comment
    1%' union select 1,username,password,4 from users-- comment
 */
import express from 'express';
import path from 'path';
import * as db from './db.js';

const app = express();

app.use(express.static(path.join(process.cwd(), 'static')));

app.set('view engine', 'ejs');

app.get('/', async (req, res) => {
  const { titleQuery } = req.query;
  let queryString;
  let options = [];

  if (titleQuery) {
    // helytelen, támadható!!!
    queryString = `select * from blogPosts where title like '%${titleQuery}%'`;

    // helyes!!!
    // query = 'select * from blogPosts where title like ?';
    // options = [titleQuery];

    // ugyancsak helyes
    // query = `select * from blogPosts where title like '%${db.escape(titleQuery)}%'`;
  } else {
    queryString = 'select * from blogPosts';
    options = [];
  }

  console.log(queryString);

  try {
    const blogPosts = await db.query(queryString, options);
    console.log(blogPosts);
    res.render('blogPosts', { blogPosts, titleQuery });
  } catch (err) {
    console.error(err);
    res.render('blogPosts', { err, titleQuery });
  }
});

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
