import express from 'express';
import path from 'path';
import fs from 'fs';

const app = express();

const rootDir = path.join(process.cwd(), 'static');

app.get('/static', (req, res) => {
  const filename = path.join(rootDir, req.query.filename);
  if (fs.existsSync(filename)) {
    res.sendFile(filename);
  } else {
    res.status(404).send('File does not exist');
  }
});

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
