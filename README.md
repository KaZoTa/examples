# Fejlesztői eszközök

A webprogramozás tantárgy laborfeladatainak sikeres megoldásához szükségünk lesz különböző szoftver eszközökre, amelyeket otthoni használat céljából telepítenünk kell a saját számítógépünkre. A legfontosabb eszközök listája a következő:

1. Legalább két modern legfrisebb verziójú böngésző: [Firefox](https://www.mozilla.org/en-US/firefox), [Chrome](https://www.google.com/chrome/), [Safari](https://safari.en.softonic.com/), [Opera](https://www.opera.com/), stb. Mivel a HTML oldalak megjelenítésében adódnak különbségek böngészőtől függően, mindenképpen érdemes a házikat több böngészőben is ellenőrizni. Legalább a legelterjedtebb böngészőket érdemes figyelembe venni.
2. Szintaxis kiemeléssel (syntax highlighting) rendelkező szövegszerkesztő: leginkább ajánlott a [Visual Studio Code](https://code.visualstudio.com/).
3. Statikus webszerver: [nginx](https://www.nginx.com/)
4. [git verziókövető](https://git-scm.com). Használati opciók:
    - konzolból a velejáró *Git Bash* segítségével;
    - a szerkesztő pluginja segítségével - [git VS Code-ból](https://code.visualstudio.com/docs/editor/versioncontrol#_git-support);
    - külső git kliensalkalmazás, pl. [SourceTree](https://www.sourcetreeapp.com/), [GitKraken](https://www.gitkraken.com/), stb.
5. HTTP hívások tesztelésére [Postman](https://www.getpostman.com/downloads/)
6. [MySQL Community](https://dev.mysql.com/downloads/mysql/) adatbáziskezelő rendszer (a későbbiekben lesz csupán rá szükség)


# Példaprogramok

Az előadás különböző tematikájához tartozó példaprogramok elérhetőek egy [publikus git tárolón](https://gitlab.com/ubb-webprog/examples) keresztül: 

## Szerkezet

- A példaprogramok tematika szerint vannak csoportosítva. 
- Minden tematikának egy külön könyvtár felel meg, ezen belül megtalálhatóak az aktuális példaprogramok.
- Ahol megjelennek, az előző évek példaprogramjai a webes standardok gyors változásának köszönhetően több esetben is elavultak, ezért használatuk **nem** ajánlott.

## Használat

### Első letöltés

- git *konzol*parancs:
    - navigáljunk azon mappába, ahova letöltenénk a példákat: `cd <celpont>`
    - letöltés: `git clone https://gitlab.com/ubb-webprog/examples` (létrehoz egy `examples` almappát)
- *VS Code*-ból:
    - Command pallette: `Ctrl+Shift+P`
    - "Git: Clone"
    - Repo URL: `https://gitlab.com/ubb-webprog/examples`

### A tartalom frissítése

- git *konzol*parancs:
    - navigáljunk azon mappába, ahova korábban letöltöttük a példákat: `cd <celpont>/examples`
    - frissítés: `git pull`
- *VS Code*-ból:  
![](vscode-sync.png){width="40%"}

### Hiba esetén

Hiba léphet fel, hogyha a lokális tárolóban levő tartalom *megváltozott* (ha kézzel változtattok a példaprogramok tartalmán). A könyvtár *tisztításáért* hívjátok a következőt ugyanazon `examples` mappából (**Vigyázat!:** Ezen parancsok meghívása a laborfeladatos tárolóban kitörölheti annak módosításait):  
```
    git reset --hard HEAD
    git clean -xfd
```

# Saját laborfeladatos tároló

- A laborfeladatokat a GitLab verziókövető portálra kell feltölteni. Minden diáknak automatikusan készül egy-egy projekt, melynek URL-je:  
  `https://gitlab.com/ubb-webprog/labs2020/lab-<abcd1234>`  
  ahol az `abcd1234` megfelel a felhaszálói SSID-toknak. Ide töltsétek fel gittel a `develop` ágra (ez lesz használva automatikusan) minden megoldást.
- Ahhoz hogy a projekthez elérést nyerjetek, [készítsetek felhaszálót a GitLabon](https://gitlab.com/users/sign_in#register-pane), majd adjátok le az itt használt **e-mail címeteket** bármely oktatónak.
- Minden laborfeladathoz tartozik egy *merge request* (automatikusan létrehozva általunk). A feladatokat **inkrementálisan** kell megoldani, tehát pl. a 2-es feladat leadására nem nyílik lehetőség, amíg az 1-es nincs elfogadva. Amiután az 1-es feladat merge requestjét lezárjuk, azután jön létre automatiksan a 2-es feladathoz tartozó merge request.
- Az értékelésnél kizárólag a GitLabra felpusholt állományokat vesszük figyelembe. A leadás dátumának az *utolsó commit* időpontja számít.


# *Bónusz:* SSH használata gittel

Annak érdekében, hogy ne kelljen minden pull/push operációkor *felhasználót és jelszót* megadj a gitnek, konfigurálható az SSH-s elérés. Ennek lépései:

1. Lokálisan a *Git Bash*-ből: `ssh-keygen` - minden kért argumentumot **üresen hagyva**.
2. `cat ~/.ssh/id_rsa.pub` - ez kiírja a publikus SSH kulcsot.
3. A profilodon a *Settings* -> *SSH Keys* részbe másold be az SSH kulcsot *teljességében* (`ssh-rsa` kulcsszó az elején, a géped neve a végén).
4. A projekt oldalán másold le az SSH-s elérési linket - ennek formája `git@gitlab.com:ubb-webprog/...`
5. A fenti HTTP-s `clone` parancs helyett: `git clone <link>` Ha már klónoztad a repót korábban, akkor annak folderén belül: `git remote set-url origin <link>`

