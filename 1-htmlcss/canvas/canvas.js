window.onload = () => {
  console.log('starting...');
  const cv = document.getElementById('cv');
  cv.addEventListener('click', (event) => {
    const x = event.pageX - cv.offsetLeft;
    const y = event.pageY - cv.offsetTop;
    console.log(`x: ${x}, y: ${y}`);
    const ct = cv.getContext('2d');
    ct.fillRect(x, y, 2, 2);
    const picture = document.getElementById('picture');
    ct.drawImage(picture, x, y, 50, 50);
  });
};
