// látható más modulokból, amelyek importálják ezt
export function libFunction1() {
  console.log('I am libfunction1');
}

// egyenértékű a fölsővel
export const libFunction2 = () => {
  console.log('I am libfunction2');
};

// modulon belül privát függvény
// eslint-disable-next-line
function privateFunction() {
  console.log('I am a non-exported function');
}
