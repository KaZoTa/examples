// Returns cookie by identifier
function cookieVal(cookieName, cookieString) {
  const startLoc = cookieString.indexOf(cookieName);
  if (startLoc === -1) {
    return ''; // There is no cookie with this identifier
  }
  const sepLoc = cookieString.indexOf('=', startLoc);
  let endLoc = cookieString.indexOf(';', startLoc);
  if (endLoc === -1) {
    // There is no semicolon after the last cookie
    endLoc = cookieString.length;
  }
  return cookieString.substring(sepLoc + 1, endLoc);
}

// Saving cookies
function storeCookies() {
  const tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  const expires = `; expires=${tomorrow}`;
  const firstName = document.getElementById('form').firstField.value;
  // Accessing form data
  const lastName = document.forms[0].lastField.value;
  document.cookie = `first=${firstName} ${expires}`;
  document.cookie = `last=${lastName} ${expires}`;
}

// Save and notify the user
// eslint-disable-next-line
function registerName() {
  storeCookies();
  // eslint-disable-next-line no-alert
  alert('Your registration finished successfully!');
}

// Presets the fields of the form
// eslint-disable-next-line
function presetValues() {
  const { firstField, lastField } = document.getElementById('form');
  const cookies = document.cookie;
  firstField.value = cookieVal('first', cookies);
  lastField.value = cookieVal('last', cookies);
}
