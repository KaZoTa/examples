/**
 * Megkeresi az összes alfolderben levő node.js projektet
 * és meghívja a megadott npm parancsot, hogy hozza naprakész állapotba a függőségeket.
 */
import fs from 'fs';
import path from 'path';
import cp from 'child_process';

// rekurzív alprogram mely keresi a 'package.json'-t tartalmazó foldereket
const getProjectFolders = (baseFolder) => {
  const ret = [];

  // ha a jelenlegi folderben van package.json, hozzáadjuk a listához
  if (fs.existsSync(path.join(baseFolder, 'package.json'))) {
    ret.push(baseFolder);
  }

  // lekérjük a folder tartalmát
  const contents = fs.readdirSync(baseFolder, { withFileTypes: true });
  // szűrjük hogy csak foldereket nézzünk, amelyek nem node_modules vagy .git
  contents.filter((file) => file.isDirectory() && !(['node_modules', '.git'].indexOf(file.name) > -1)).forEach((dir) => {
    // rekurzió
    ret.push(...getProjectFolders(path.join(baseFolder, dir.name)));
  });

  return ret;
};

const executeInProjectFolders = (cmd) => {
  getProjectFolders(process.cwd()).forEach((projectFolder) => {
    console.log(`Calling "${cmd.join(' ')}" in project folder: ${projectFolder}`);
    cp.spawn(cmd[0], cmd.slice(1), { env: process.env, cwd: projectFolder, stdio: 'inherit' });
  });
};

// milyen parancsot futtatunk minden projektben
let cmd = ['npm', 'i'];
if (process.argv.length > 2) {
  cmd = process.argv.slice(2);
}

executeInProjectFolders(cmd);
