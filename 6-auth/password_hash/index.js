/*
Ez a példa egyirányú hashelést végez jelszavak titkosítására.
*/
import express from 'express';
import crypto from 'crypto';
import util from 'util';

const hashSize = 30,
  saltSize = 30,
  hashAlgorithm = 'sha512',
  iterations = 1000;

const pbkdf2 = util.promisify(crypto.pbkdf2);

const app = express();
app.use(express.urlencoded({ extended: true }));

// generálunk hash-t egy jelszóból
app.post('/create_hash', async (req, res) => {
  const { password } = req.body;
  // só generálása
  const salt = crypto.randomBytes(saltSize);
  // hash készítése
  const hash = await pbkdf2(password, salt, iterations, hashSize, hashAlgorithm);
  // konkatenálás és hexa stringgé alakítás
  const hashWithSalt = `${hash.toString('base64')}:${salt.toString('base64')}`;
  // a konkatenált hash-t és sót tárolnánk adatbázisban
  res.send(hashWithSalt);
});

// ellenőrizzük egy megadott jelszóról hogy megfelel-e
// egy megadott hashnek
app.post('/check_hash', async (req, res) => {
  // a konkatenált hash-t és sót adatbázisból kérnénk le
  const { password, hashWithSalt } = req.body;
  // hexa string dekódolás és dekonkatenálás
  const [expectedHashB64, saltB64] = hashWithSalt.split(':');
  const salt = Buffer.from(saltB64, 'base64');
  // újra-hash-elés
  const actualHash = await pbkdf2(password, salt, iterations, hashSize, hashAlgorithm);
  // hexa stringgé alakítás
  const actualHashB64 = actualHash.toString('base64');

  if (expectedHashB64 === actualHashB64) {
    res.send('Passwords match');
  } else {
    res.status(401).send('Passwords do not match');
  }
});

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
