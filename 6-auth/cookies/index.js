/* Egyszerű példa egy HTTP szerverre amely három elérési útra válaszol:
1. /setcookie - válaszban kéri a böngészőtől egy süti beállítását
2. /getcookies - kiirja console-ra a kérés fejlécét
3. /deletecookie - utasítja a klienst a süti törlésére

felhasznált modulok:
  express: web szerver
  cookie-parser: feldolgozza a kérés fejlécben érkező cookie-kat
                és elérhetővé teszi a request.cookie objektumon keresztül
*/
import express from 'express';
import cookieParser from 'cookie-parser';

const app = express();

// sütiket feldolgozó middleware HTTP hívásokból
// beállítja a req.cookies adattagot
app.use(cookieParser());

// beállít egy sütit
app.get('/setcookie', (req, res) => {
  console.log('Sending cookie to the client');
  // a válaszban utasítja a klienst, hogy mentse el a megadott cookie-t
  res.cookie('mycookie', 'mycookievalue');
  res.send('Received command to set a cookie');
});

// kiírja az aktív sütiket
app.get('/getcookies', (req, res) => {
  console.log('Received the following cookies:');
  Object.entries(req.cookies).forEach(([cookieName, cookieValue]) => {
    console.log(`  ${cookieName} : ${cookieValue}`);
  });
  res.send('Received request, showing cookies on server side console');
});

// törli a fent megadott sütit
app.get('/deletecookie', (req, res) => {
  console.log('Deleting cookie');
  res.clearCookie('mycookie');
  res.send('Cookie cleaned');
});

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
