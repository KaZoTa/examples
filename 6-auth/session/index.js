/* Példaprogram, amely egy session változóban számolja, hogy
egy adott felhasználó hányszor látogatott meg bizonyos oldalakat

Figyelem: ez a példa az alapértelmezett (memóriában való) sesszió
tárolási mechanizmust használja, ami csak fejlesztés során hasznlható.
A példa az express-session github project oldaláról származik.
*/
import express from 'express';
import session from 'express-session';

const app = express();

// session middleware beállítása
app.use(session({
  secret: '142e6ecf42884f03',
  resave: false,
  saveUninitialized: true,
}));

// elérés-számláló
app.use((req, res, next) => {
  // views objektum inicializálása, ha ez még nem történt meg
  req.session.views = req.session.views || {};

  const { views } = req.session;
  const pathname = req.path;

  // számláljuk az elérést
  views[pathname] = (views[pathname] || 0) + 1;

  next();
});

app.all('/reset_session', (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      res.status(500).send(`Session reset error: ${err.message}`);
    } else {
      res.send('Your session has been destroyed');
    }
  });
});

app.use((req, res) => {
  const { id, views } = req.session;
  const pathname = req.path;

  console.log(`Session ID: ${id}, path: ${pathname}, view count: ${views[pathname]}`);
  res.send(`You viewed this page (${pathname}) ${views[pathname]} times`);
});

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
