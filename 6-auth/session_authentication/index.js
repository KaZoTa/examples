import express from 'express';
import session from 'express-session';

// ezek az értékek adatbázisból jöjjenek
const users = {
  egyuser: 'egyjelszo',
  masikuser: 'masikjelszo',
};

const app = express();

// session middleware beállítása
app.use(session({
  secret: '142e6ecf42884f03',
  resave: false,
  saveUninitialized: true,
}));
app.use(express.urlencoded({ extended: true }));

// belépés
app.post('/login', (req, res) => {
  const { username, password } = req.body;
  if (username && password && users[username] === password) {
    req.session.username = username;
    res.send('Login successful');
  } else {
    res.status(401).send('Wrong credentials');
  }
});

// kilépés
app.post('/logout', (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      res.status(500).send(`Session reset error: ${err.message}`);
    } else {
      res.send('Logout successful');
    }
  });
});

// session auth middleware
app.use((req, res, next) => {
  if (req.session.username) {
    next();
  } else {
    res.status(401).send('Not logged in');
  }
});

// Minden hívás bejelentkezést kényszerít
app.get('/*', (req, res) => {
  res.send('Welcome, authenticated user!');
});

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
