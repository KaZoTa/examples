/*
Ez a példa egyirányú hashelést végez jelszavak titkosítására.
*/
import express from 'express';
import bcrypt from 'bcrypt';

const app = express();
app.use(express.urlencoded({ extended: true }));

// generálunk hash-t egy jelszóból
app.post('/create_hash', async (req, res) => {
  const { password } = req.body;
  // hash készítése (sózást megoldja)
  const hashWithSalt = await bcrypt.hash(password, 10);
  // a konkatenált hash-t és sót tárolnánk adatbázisban
  res.send(hashWithSalt);
});

// ellenőrizzük egy megadott jelszóról hogy megfelel-e
// egy megadott hashnek
app.post('/check_hash', async (req, res) => {
  // a konkatenált hash-t és sót adatbázisból kérnénk le
  const { password, hashWithSalt } = req.body;
  // jelszó ellenőrzése bcrypttel
  const match = await bcrypt.compare(password, hashWithSalt);

  if (match) {
    res.send('Passwords match');
  } else {
    res.status(401).send('Passwords do not match');
  }
});

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
