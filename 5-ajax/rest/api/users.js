import express from 'express';
import * as userDao from '../db/users.js';
import * as blogPostDao from '../db/blogPosts.js';
import hasProps from '../middleware/validate.js';

const router = express.Router();

// findAll
router.get('/', (req, res) => {
  userDao.findAllUsers()
    .then((users) => res.json(users))
    .catch((err) => res.status(500).json({ message: `Error while finding all users: ${err.message}` }));
});

// findById
router.get('/:userId', (req, res) => {
  const { userId } = req.params;
  userDao.findUserById(userId)
    .then((user) => (user ? res.json(user) : res.status(404).json({ message: `User with ID ${userId} not found.` })))
    .catch((err) => res.status(500).json({ message: `Error while finding user with ID ${userId}: ${err.message}` }));
});

// findBlogPostsForUser
router.get('/:userId/blogPosts', (req, res) => {
  const { userId } = req.params;
  userDao.userExists(userId)
    .then((exists) => {
      if (!exists) {
        return res.status(404).json({ message: `User with ID ${userId} not found.` });
      }
      return blogPostDao.findBlogPostsByAuthorId(userId)
        .then((blogPosts) => res.json(blogPosts));
    })
    .catch((err) => res.status(500).json({ message: `Error while finding blog posts for user with ID ${userId}: ${err.message}` }));
});

// insert
router.post('/', hasProps(['fullName']), (req, res) => {
  userDao.insertUser(req.body)
    .then((user) => res.status(201).location(`${req.fullUrl}/${user.id}`).json(user))
    .catch((err) => res.status(500).json({ message: `Error while creating user: ${err.message}` }));
});

// delete
router.delete('/:userId', (req, res) => {
  const { userId } = req.params;
  userDao.deleteUser(userId)
    .then((rows) => (rows ? res.sendStatus(204) : res.status(404).json({ message: `User with ID ${userId} not found.` })))
    .catch((err) => res.status(500).json({ message: `Error while deleting user with ID ${userId}: ${err.message}` }));
});

// update puttal
router.put('/:userId', hasProps(['fullName']), (req, res) => {
  const { userId } = req.params;
  userDao.updateOrInsertUser(userId, req.body)
    .then(() => res.sendStatus(204))
    .catch((err) => res.status(500).json({ message: `Error while updating user with ID ${userId}: ${err.message}` }));
});

// update patch-csel
router.patch('/:userId', (req, res) => {
  const { userId } = req.params;
  userDao.updateUser(userId, req.body)
    .then((rows) => (rows ? res.sendStatus(204) : res.status(404).json({ message: `User with ID ${userId} not found.` })))
    .catch((err) => res.status(500).json({ message: `Error while updating user with ID ${userId}: ${err.message}` }));
});

export default router;
