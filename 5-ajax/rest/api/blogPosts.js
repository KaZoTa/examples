import express from 'express';
import * as blogPostDao from '../db/blogPosts.js';
import * as userDao from '../db/users.js';
import hasProps from '../middleware/validate.js';

const router = express.Router();

// findAll
router.get('/', (req, res) => {
  blogPostDao.findAllBlogPosts()
    .then((blogPosts) => res.json(blogPosts))
    .catch((err) => res.status(500).json({ message: `Error while finding all blog posts: ${err.message}` }));
});

// findById
router.get('/:blogPostId', (req, res) => {
  const { blogPostId } = req.params;
  blogPostDao.findBlogPostById(blogPostId)
    .then((blogPost) => (blogPost ? res.json(blogPost) : res.status(404).json({ message: `Blog post with ID ${blogPostId} not found.` })))
    .catch((err) => res.status(500).json({ message: `Error while finding blog post with ID ${blogPostId}: ${err.message}` }));
});

// findById => author
router.get('/:blogPostId/author', (req, res) => {
  const { blogPostId } = req.params;
  userDao.findAuthorByBlogPostId(blogPostId)
    .then((user) => (user ? res.json(user) : res.status(404).json({ message: `Author of blog post with ID ${blogPostId} not found.` })))
    .catch((err) => res.status(500).json({ message: `Error while finding author for blog post with ID ${blogPostId}: ${err.message}` }));
});

// insert
router.post('/', hasProps(['title', 'content', 'authorId']), (req, res) => {
  userDao.userExists(req.body.authorId)
    .then((exists) => {
      if (!exists) {
        return res.status(400).json({ message: `User with ID ${req.body.authorId} does not exist` });
      }

      return blogPostDao.insertBlogPost(req.body)
        .then((blogPost) => res.status(201).location(`${req.fullUrl}/${blogPost.id}`).json(blogPost));
    })
    .catch((err) => res.status(500).json({ message: `Error while creating blog post: ${err.message}` }));
});

// delete
router.delete('/:blogPostId', (req, res) => {
  const { blogPostId } = req.params;
  blogPostDao.deleteBlogPost(blogPostId)
    .then((rows) => (rows ? res.sendStatus(204) : res.status(404).json({ message: `BlogPost with ID ${blogPostId} not found.` })))
    .catch((err) => res.status(500).json({ message: `Error while deleting blog post with ID ${blogPostId}: ${err.message}` }));
});

// update puttal
router.put('/:blogPostId', hasProps(['title', 'content', 'authorId']), (req, res) => {
  const { blogPostId } = req.params;
  blogPostDao.updateOrInsertBlogPost(blogPostId, req.body)
    .then(() => res.sendStatus(204))
    .catch((err) => res.status(500).json({ message: `Error while updating blog post with ID ${blogPostId}: ${err.message}` }));
});

// update patch-csel
router.patch('/:blogPostId', (req, res) => {
  const { blogPostId } = req.params;
  blogPostDao.updateBlogPost(blogPostId, req.body)
    .then((rows) => (rows ? res.sendStatus(204) : res.status(404).json({ message: `BlogPost with ID ${blogPostId} not found.` })))
    .catch((err) => res.status(500).json({ message: `Error while updating blog post with ID ${blogPostId}: ${err.message}` }));
});

export default router;
