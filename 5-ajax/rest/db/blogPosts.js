// A blogPost entitással kapcsolatos DB műveletek.
import dbConnection from './connection.js';

// táblázat létrehozása
(async () => {
  await dbConnection.executeQuery('DROP TABLE IF EXISTS blogPosts');
  await dbConnection.executeQuery(`
    CREATE TABLE IF NOT EXISTS blogPosts (
      id BIGINT NOT NULL AUTO_INCREMENT,
      title VARCHAR(63),
      content VARCHAR(1023),
      authorId BIGINT,
      date VARCHAR(63),
      PRIMARY KEY (id)
    )
  `);
})();

// összes sor visszatérítése
export const findAllBlogPosts = () => (
  dbConnection.executeQuery('SELECT * FROM blogPosts')
);

// ID alapján keresés
// Tömböt térít vissza, ennek csak első elemét adjuk vissza (undefined ha a lista üres)
export const findBlogPostById = async (blogPostId) => {
  const blogPosts = await dbConnection.executeQuery('SELECT * FROM blogPosts WHERE id=?', [blogPostId]);
  return blogPosts[0];
};

// Szerző ID-ja szerinti keresés
export const findBlogPostsByAuthorId = (authorId) => (
  dbConnection.executeQuery('SELECT * FROM blogPosts WHERE authorId=?', [authorId])
);

// Beszúrás generált ID-val
// Visszatérítjük az entitás állapotát generált ID-val
export const insertBlogPost = async (blogPost) => {
  const date = new Date().toISOString();
  const result = await dbConnection.executeQuery(
    'INSERT INTO blogPosts VALUES (default,?,?,?,?)',
    [blogPost.title, blogPost.content, blogPost.authorId, date],
  );

  return {
    id: result.insertId,
    title: blogPost.title,
    content: blogPost.content,
    authorId: blogPost.authorId,
    date,
  };
};

// Törlés
// Kikérjük a módosított sorok számát (innen tudjuk hogy létezett-e)
export const deleteBlogPost = async (blogPostId) => {
  const result = await dbConnection.executeQuery('DELETE FROM blogPosts WHERE id=?', [blogPostId]);
  return result.affectedRows > 0;
};

// Módosítás vagy beszúrás
// Visszatérítjük az entitás teljes állapotát
export const updateOrInsertBlogPost = async (blogPostId, blogPost) => {
  const date = new Date().toISOString();
  const result = await dbConnection.executeQuery(
    'INSERT INTO blogPosts VALUES (?,?,?,?,?) ON DUPLICATE KEY UPDATE ?',
    [blogPostId, blogPost.title, blogPost.content, blogPost.authorId, date, blogPost],
  );

  return {
    id: result.insertId,
    title: blogPost.title,
    content: blogPost.content,
    authorId: blogPost.authorId,
    date,
  };
};

// Módosítás
// Kikérjük a módosított sorok számát (innen tudjuk hogy létezik-e)
export const updateBlogPost = async (blogPostId, blogPost) => {
  const result = await dbConnection.executeQuery('UPDATE blogPosts SET ? WHERE id=?', [blogPost, blogPostId]);
  return result.affectedRows > 0;
};
