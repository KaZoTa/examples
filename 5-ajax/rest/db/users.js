// A user entitással kapcsolatos DB műveletek.
import dbConnection from './connection.js';

// táblázat létrehozása
(async () => {
  await dbConnection.executeQuery('DROP TABLE IF EXISTS users');
  await dbConnection.executeQuery(`
    CREATE TABLE IF NOT EXISTS users (
      id BIGINT NOT NULL AUTO_INCREMENT,
      fullName VARCHAR(63),
      PRIMARY KEY (id)
    )
  `);
})();

// összes sor visszatérítése
export const findAllUsers = () => (
  dbConnection.executeQuery('SELECT * FROM users')
);

// ID alapján keresés
// Tömböt térít vissza, ennek csak első elemét adjuk vissza (undefined ha a lista üres)
export const findUserById = async (userId) => {
  const users = await dbConnection.executeQuery('SELECT * FROM users WHERE id=?', [userId]);
  return users[0];
};

// BlogPost ID alapján keresés
export const findAuthorByBlogPostId = async (blogPostId) => {
  const users = await dbConnection.executeQuery(
    'SELECT * FROM users WHERE id=(SELECT authorId FROM blogPosts WHERE id=?)',
    [blogPostId],
  );
  return users[0];
};

// ID alapján verifikáció hogy user létezik-e
// Ezalapján hozzáköthetünk új blog postokat anélkül hogy betöltsük memóriába.
export const userExists = async (userId) => {
  const result = await dbConnection.executeQuery('SELECT COUNT(*) AS count FROM users WHERE id=?', [userId]);
  return result[0].count > 0;
};

// Beszúrás generált ID-val
// Visszatérítjük az entitás állapotát generált ID-val
export const insertUser = async (user) => {
  const result = await dbConnection.executeQuery('INSERT INTO users VALUES (default, ?)', [user.fullName]);
  return {
    id: result.insertId,
    fullName: user.fullName,
  };
};

// Törlés
// Továbbítjuk hogy létezett-e az entitás (érintettünk-e több mint 0 sort)
export const deleteUser = async (userId) => {
  const result = await dbConnection.executeQuery('DELETE FROM users WHERE id=?', [userId]);
  return result.affectedRows > 0;
};

// Módosítás vagy beszúrás
// Visszatérítjük az entitás teljes állapotát
export const updateOrInsertUser = async (userId, user) => {
  const result = await dbConnection.executeQuery(
    'INSERT INTO users VALUES (?, ?) ON DUPLICATE KEY UPDATE fullName=?',
    [userId, user.fullName, user.fullName],
  );
  return {
    id: result.insertId,
    fullName: user.fullName,
  };
};

// Módosítás
// Kikérjük a módosított sorok számát (innen tudjuk hogy létezik-e)
export const updateUser = async (userId, user) => {
  const result = await dbConnection.executeQuery(
    'UPDATE users SET fullName=? WHERE id=?',
    [user.fullName, userId],
  );
  return result.affectedRows > 0;
};
