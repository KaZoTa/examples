import express from 'express';
import path from 'path';
import apiRoutes from './api/index.js';

const app = express();

// statikus frontend
app.use(express.static(path.join(process.cwd(), 'static')));

// az API-t bekötjük a /api... endpointra
app.use('/api', apiRoutes);

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
