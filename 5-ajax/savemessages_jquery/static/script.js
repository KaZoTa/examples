/* eslint-disable */
function getMessages() {
  // küldünk egy GET kérést, s megadjuk a callbacket
  $.get('/messages', (messages) => {
    const formattedMessages = messages.map(message =>
      `message: ${message.text}, date: ${message.date}`).join('\n');
    
    // beállítjuk a "messages" ID-jú input értékét a formatált stringre
    $('#messages').val(formattedMessages);
  });
}

// meghívjuk a getMessages-t a dokumentum betöltésekor
$(getMessages);

// ha click érkezik az adott ID-jű gombra
$('#sendMessageBtn').click(() => {

  // küldünk egy POST kérést
  $.ajax('/messages', {
    method: 'POST',
    contentType: 'application/json',
    data: JSON.stringify({
      text: document.getElementById('newMessage').value,
      date: new Date(),
    }),
    success: getMessages, // callback: újra lekérjük az üzeneteket
  });
});
