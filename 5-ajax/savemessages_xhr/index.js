import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import path from 'path';

const app = express();

// karbantartunk egy lista üzenetet
const messages = [];

app.use(morgan('tiny'));
app.use(express.static(path.join(process.cwd(), 'static')));

// ha kérés-body-ban JSON van, dekódoljuk
app.use(bodyParser.json());

// GET = visszaküldjük az összes eddigi üzenetet JSON-ban
app.get('/messages', (req, res) => {
  res.json(messages);
});

// POST = új üzenet beszúrása
app.post('/messages', (req, res) => {
  messages.push(req.body);
  res.send('Message inserted successfully');
});

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
