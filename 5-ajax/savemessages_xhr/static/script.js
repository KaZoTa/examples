/* eslint-disable */
function getMessages() {
  const xhr = new XMLHttpRequest();

  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4 && xhr.status === 200) {
      const messages = JSON.parse(xhr.responseText);
      const formattedMessages = messages.map(message => `message: ${message.text}, date: ${message.date}`).join('\n');
      document.getElementById('messages').value = formattedMessages;
    }
  };

  xhr.open('GET', '/messages');
  xhr.send();
}

function sendMessage() {
  const xhr = new XMLHttpRequest();

  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4 && xhr.status === 200) {
      alert(xhr.responseText);
      getMessages();
    }
  };

  xhr.open('POST', '/messages');
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify({ 
    text: document.getElementById('newMessage').value,
    date: new Date(),
  }));
}
