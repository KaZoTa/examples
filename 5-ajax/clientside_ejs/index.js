import express from 'express';
import { join } from 'path';

const app = express();

/**
 * static - HTML és kliensoldali JS
 */
app.use(express.static(join(process.cwd(), 'static')));

/**
 * kliensoldali EJS sablon-darabok a browserviews folderben
 */
app.use('/views', express.static(join(process.cwd(), 'browserviews')));

/**
 * Modell visszaadása (API)
 */
app.get('/model', (req, res) => {
  res.send({
    name: 'Alan',
    hometown: 'Somewhere, TX',
    kids: [
      { name: 'Jimmy', age: '12' },
      { name: 'Sally', age: '4' },
      { name: 'Johnny' },
    ],
  });
});

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
