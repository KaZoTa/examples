import express from 'express';
import path from 'path';

const app = express();

app.use(express.static(path.join(process.cwd(), 'static')));

app.get('/message', (req, res) => {
  res.send('Hello from the server');
});

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
