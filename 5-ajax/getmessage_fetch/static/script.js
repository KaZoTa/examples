// eslint-disable-next-line
function getMessage() {
  fetch('/message')
    .then((response) => response.text()) // 1. promise - válasz szövegének kinyerése
    .then((message) => {                 // 2. promise - szöveg használata
      document.getElementById('message').innerText = message;
    });
}
