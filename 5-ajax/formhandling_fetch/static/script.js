// eslint-disable-next-line
function submitForm(event) {

  // az esemény eredeti lekezelését (szinkron leadás) fölülírjuk
  event.preventDefault();

  // az esemény a formra van értelmezve
  const form = event.target;
  // kivesszük a form adatait
  const formData = new FormData(form);

  // aszinkron kérést küldünk
  fetch('/submit_form', {
    method: 'POST',
    body: formData,
  }).then((response) => response.text())
    .then((responseText) => {
      document.getElementById('result').value = responseText;
    });
}
