/*
Form adatok lekezelését végzi.
A statikus mappában adott egy HTML egy formmal - ezt szolgáljuk fel GET híváskor a gyökérre
Az ottani form leadását feldolgozzuk itt express segítségével.

Ez egy kiegészítése a 3-nodejs formhandling példának ASZINKRON hívások küldésére.
*/

import express from 'express';
import path from 'path';
import eformidable from 'express-formidable';

const app = express();

// a static mappából adjuk a HTML állományokat
app.use(express.static(path.join(process.cwd(), 'static')));

// formidable-lel dolgozzuk fel a kéréseket
app.use(eformidable());

// formfeldolgozás
app.post('/submit_form', (request, response) => {
  const birthDate = new Date(request.fields.birthdate);
  const knowsHtml = Boolean(request.fields.knowshtml);

  const respBody = `A szerver sikeresen megkapta a következő információt:
    név: ${request.fields.name}
    jelszó: ${request.fields.password} (sosem szabad ezt így kiírni :))
    születési dátum: ${birthDate}
    nem: ${request.fields.gender}
    tud HTML-t: ${knowsHtml}
  `;
  console.log(respBody);

  response.set('Content-Type', 'text/plain;charset=utf-8');
  response.end(respBody);
});

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
