/* eslint-disable */
async function getMessages() {
  const response = await fetch('/messages');
  const messages = await response.json();
  const formattedMessages = messages.map(message => `message: ${message.text}, date: ${message.date}`).join('\n');
  document.getElementById('messages').value = formattedMessages;
}

function sendMessage() {
  fetch('/messages', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      text: document.getElementById('newMessage').value,
      date: new Date(),
    }),
  }).then(response => response.text())
    .then(getMessages);
}
