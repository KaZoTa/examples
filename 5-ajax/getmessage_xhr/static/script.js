// eslint-disable-next-line
function getMessage() {
  const xhr = new XMLHttpRequest();

  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4 && xhr.status === 200) {
      document.getElementById('message').innerText = xhr.responseText;
    }
  };

  xhr.open('GET', '/message');
  xhr.send();
}
