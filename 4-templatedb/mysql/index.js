/*
Csatlakozik egy MySQL adatbázishoz egy connection segítségével.
Minden HTTP kérés érkezése esetén beírja az adatbázisba
a kérés metódusát, az URL-t és a kérés időpontját.

Figyelem! npm i csak a MySQL-hez csatlakoztató segédmodult telepíti, nem magát a MySQL-t.
*/
import express from 'express';
import mysql from 'mysql';

// létesít egy kapcsolatot az adatbázissal
// a megadott DB-nek/felhasználónak léteznie kell
// létre lehet hozni a mellékelt setup.sql szkript segítségével
const connection = mysql.createConnection({
  database: 'webprog',
  host: 'localhost',
  port: 3306,
  user: 'webprog',
  password: 'VgJUjBd8',
});

connection.connect((err) => {
  if (err) {
    console.error(`Connection error: ${err.message}`);
    process.exit(1);
  }

  // létrehozzunk a táblázatot, ha még nem létezik
  connection.query(`CREATE TABLE IF NOT EXISTS requests (
        method varchar(20),
        url varchar(50),
        date varchar(50));`, (createErr) => {
    if (createErr) {
      console.error(`Create table error: ${createErr.message}`);
      process.exit(1);
    } else {
      console.log('Table created successfully');
    }
  });
  console.log(`Connected: ${connection.threadId}`);
});

const app = express();

app.use((req, res) => {
  const { method, url } = req;
  const date = new Date().toISOString();

  // felépítjük a végrehajtandó SQL lekérdezést
  const query = `INSERT INTO requests VALUES ("${method}", "${url}", "${date}");`;
  console.log(`Executing query ${query}`);

  // végrehatjuk a lekérdezést
  connection.query(query, (err) => {
    if (err) {
      res.status(500).send(`Insertion unsuccessful: ${err.message}`);
    } else {
      res.send('Insertion successful');
    }
  });
});

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
