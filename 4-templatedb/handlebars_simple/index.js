// példa a handlebars használatára
import handlebars from 'handlebars';

// előre kompiláljuk sablonunkat
const template = handlebars.compile(`
  <p>Hello, my name is {{ name }}.</p>
  <p>I am from {{ hometown }}.</p>
  <p>I have {{ kids.length }} kids:</p>
  <ul>{{#each kids}}{{#if age}}
    <li>{{ name }} is {{ age }}</li>{{ else }}
    <li>I don't remember how old {{ name }} is.</li>{{/if}}{{/each}}
  </ul>
`);

// megadunk egy modellt (dinamikus tartalom)
const model = {
  name: 'Alan',
  hometown: 'Somewhere, TX',
  kids: [
    { name: 'Jimmy', age: '12' },
    { name: 'Sally', age: '4' },
    { name: 'Johnny' },
  ],
};

// a sablon renderelése
const view = template(model);

console.log(view);
