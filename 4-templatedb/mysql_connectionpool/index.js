/*
Csatlakozik egy MySQL adatbázishoz egy connection pool segítségével.
Minden HTTP kérés érkezése esetén beírja az adatbázisba
a kérés metódusát, az URL-t és a kérés időpontját.

Figyelem! npm i csak a MySQL-hez csatlakoztató segédmodult telepíti, nem magát a MySQL-t.
*/
import express from 'express';
import mysql from 'mysql';

// Létrehozunk egy connection poolt
const pool = mysql.createPool({
  connectionLimit: 10,
  database: 'webprog',
  host: 'localhost',
  port: 3306,
  user: 'webprog',
  password: 'VgJUjBd8',
});

// létrehozzunk a táblázatot, ha még nem létezik
// a pool.query kér egy kapcsolatot a poolból
pool.query(`CREATE TABLE IF NOT EXISTS requests (
    method varchar(20),
    url varchar(50),
    time varchar(50));`, (err) => {
  if (err) {
    console.error(`Create table error: ${err.message}`);
    process.exit(1);
  } else {
    console.log('Table created successfully');
  }
});

const app = express();

app.use((req, res) => {
  // kérünk egy kapcsolatot a pooltól
  pool.getConnection((err, connection) => {
    if (err) {
      connection.release();
      res.status(500).send(`Error in DB connection: ${err.message}`);
      return;
    }

    const { method, url } = req;
    const date = new Date().toISOString();

    // felépítjük a végrehajtandó SQL lekérdezést
    const query = `INSERT INTO requests VALUES ("${method}", "${url}", "${date}");`;
    console.log(`Executing query ${query}`);

    // végrehatjuk a lekérdezést
    connection.query(query, (queryErr) => {
      if (queryErr) {
        res.status(500).send(`Insertion unsuccessful: ${err.message}`);
      } else {
        res.send('Insertion successful');
      }
      // visszahelyezi a kapcsolatobjektumot a készletbe, így az ismét használható mások által is
      connection.release();
    });
  });
});

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
