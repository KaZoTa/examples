import { insertRequest } from '../db/db.js';

// loggoljunk minden kérést az adatbázisba, mint middleware
export default (req, res, next) => {
  insertRequest(req, (err) => {
    if (err) {
      res.status(500).render('error', { message: `Insertion unsuccessful: ${err.message}` });
    } else {
      next();
    }
  });
};
