// Moduláris express router létrehozása

import express from 'express';
import * as db from '../db/db.js';

const router = express.Router();

// gyökérre vagy /indexre érkezett GET kérésre rendereljük a korábbi hívásokat
router.get(['/', '/index'], (req, res) => {
  db.findAllRequests((err, requests) => {
    if (err) {
      res.status(500).render('error', { message: `Selection unsuccessful: ${err.message}` });
    } else {
      // kirajzoljuk a requests sablont - a model az query eredménye
      res.render('requests', { requests });
    }
  });
});

// /delete-re érkezett POST esetén töröljük a tábla tartalmaát
router.post('/delete', (req, res) => {
  db.deleteAllRequests((err) => {
    if (err) {
      res.status(500).render('error', { message: `Deletion unsuccessful: ${err.message}` });
    } else {
      // siker esetén visszairányítunk a lekérési oldalra
      res.render('requests', { requests: [] });
    }
  });
});

export default router;
