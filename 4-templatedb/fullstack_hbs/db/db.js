// Adatbázis műveleteket végző modul

import mysql from 'mysql';

// Létrehozunk egy connection poolt
const pool = mysql.createPool({
  connectionLimit: 10,
  database: 'webprog',
  host: 'localhost',
  port: 3306,
  user: 'webprog',
  password: 'VgJUjBd8',
});

// létrehozzunk a táblázatot, ha még nem létezik
// a pool.query kér egy kapcsolatot a poolból
pool.query(`CREATE TABLE IF NOT EXISTS requests (
    method varchar(20),
    url varchar(50),
    date varchar(50));`, (err) => {
  if (err) {
    console.error(`Create table error: ${err}`);
    process.exit(1);
  } else {
    console.log('Table created successfully');
  }
});

// service metódus - beszúr egy DB sort
// majd callback-re reagál
export const insertRequest = (req, callback) => {
  const date = new Date().toISOString();
  const query = `INSERT INTO requests VALUES (
    "${req.method}", "${req.url}", "${date}");`;
  pool.query(query, callback);
};

// service metódus - lekéri az összes sort
export const findAllRequests = (callback) => {
  const query = 'SELECT * FROM requests';
  pool.query(query, callback);
};

// service metódus - törli az összes sort
export const deleteAllRequests = (callback) => {
  const query = 'DELETE FROM requests';
  pool.query(query, callback);
};
