// Adatbázis műveleteket végző modul

import sql from 'mssql';

// Létrehozunk egy connection poolt
const connectionConfig = {
  server: 'localhost',
  user: 'webprog',
  password: 'VgJUjBd8',
  database: 'webprog',
  options: {
    trustServerCertificate: true,
  },
};

const pool = new sql.ConnectionPool(connectionConfig);

(async () => {
  await pool.connect();

  const createQuery = `
  IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='requests' and xtype='U')
    CREATE TABLE requests (
      method varchar(20),
      url varchar(50),
      date varchar(50)
    )`;

  await pool.query(createQuery);
  console.log('Table exists successfully');
})();

// service metódus - beszúr egy DB sort
// majd callback-re reagál
export const insertRequest = (req) => {
  const date = new Date().toISOString();
  const query = `INSERT INTO requests VALUES (
    '${req.method}', '${req.url}', '${date}')`;
  return pool.query(query);
};

// service metódus - lekéri az összes sort
export const findAllRequests = async () => {
  const query = 'SELECT * FROM requests';
  const data = await pool.query(query);
  return 'recordset' in data ? data.recordset : [];
};

// service metódus - törli az összes sort
export const deleteAllRequests = () => {
  const query = 'DELETE FROM requests';
  return pool.query(query);
};
