// Moduláris express router létrehozása

import express from 'express';
import * as db from '../db/db.js';

const router = express.Router();

// gyökérre vagy /indexre érkezett GET kérésre rendereljük a korábbi hívásokat
router.get(['/', '/index'], async (req, res) => {
  try {
    const requests = await db.findAllRequests();
    // kirajzoljuk a requests sablont - a model az query eredménye
    res.render('requests', { requests });
  } catch (err) {
    res.status(500).render('error', { message: `Selection unsuccessful: ${err.message}` });
  }
});

// /delete-re érkezett POST esetén töröljük a tábla tartalmaát
router.post('/delete', async (req, res) => {
  try {
    await db.deleteAllRequests();
    // siker esetén visszairányítunk a lekérési oldalra
    res.render('requests', { requests: [] });
  } catch (err) {
    res.status(500).render('error', { message: `Deletion unsuccessful: ${err.message}` });
  }
});

export default router;
