/*
Példa egy egyszerű web-szerverre amely HTTP kéréseket fogad és kiírja a
console-ra a kérés metódusát, az URL-t, valamint a kérés fejlécét

Handlebars template engine-t használ a hbs express middleware-rel
Figyelem!: noha nem hívunk "import"-ot rá, a "hbs" függőség szükséges
  az express app helyes beállításához.
*/

import express from 'express';
import path from 'path';
import { engine } from 'express-handlebars';
import helpers from 'handlebars-helpers';

const app = express();

// beállítjuk a hbs engine-t (még nincs)
app.set('view engine', 'hbs');
app.set('views', path.join(process.cwd(), 'views'));

// deklaráljuk az engine-t mivel nincs natív összekötés
app.engine('hbs', engine({
  extname: 'hbs',
  helpers: helpers(),
}));

app.use((request, response) => {
  // beállítjuk a view modelljét
  const model = {
    method: request.method,
    url: request.url,
    headers: request.headers,
    layout: false, // ez a példa nem használ layoutot
  };

  // views/requestinfo.hbs sablonnal
  // felépítjük a választ a kliensnek
  response.render('requestinfo', model);
});

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
