// példa a ejs használatára
import ejs from 'ejs';

// előre kompiláljuk sablonunkat
const template = ejs.compile(`
  <p>Hello, my name is <%= name %>.</p>
  <p>I am from <%= hometown %>.</p>
  <p>I have <%= kids.length %> kids:</p>
  <ul><% kids.forEach((kid) => { if (kid.age) { %>
    <li><%= kid.name %> is <%= kid.age %></li><% } else { %>
    <li>I don't remember how old <%= kid.name %> is.</li><% }}) %>
  </ul>
`);

// megadunk egy modellt (dinamikus tartalom)
const model = {
  name: 'Alan',
  hometown: 'Somewhere, TX',
  kids: [
    { name: 'Jimmy', age: '12' },
    { name: 'Sally', age: '4' },
    { name: 'Johnny' },
  ],
};

// a sablon renderelése
const view = template(model);

console.log(view);
