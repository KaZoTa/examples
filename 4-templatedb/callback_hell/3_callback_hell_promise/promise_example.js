/**
 * Promise objektum az aszinkron hívások bevárásához.
 * Az alábbi programban számlálókkal fogjuk szemléltetni az aszinkron hívásokat.
 * A program két számlálót indít el, mindkettő 5 másodpercig vár.
 * A második számláló csak akkor fog elindulni ha az első számláló már megállt.
 *
 * A programot a node_example.js paranccsal tudjuk futtatni.
 */

function waitFiveSeconds(callback) {
  console.log('Waiting 5 seconds.');
  setTimeout(() => {
    console.log('5 seconds passed');
    callback();
  }, 5000);
}

function asyncProcess() {
  return new Promise((resolve) => {
    waitFiveSeconds(resolve);
  });
}

function main() {
  asyncProcess().then(asyncProcess);
}

main();
