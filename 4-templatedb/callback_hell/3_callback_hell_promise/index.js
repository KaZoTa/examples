/**
 * Az alábbi példaprogram az 2_callback_hell_function_handler katalógusban levő megoldást
 * egészíti ki oly módon, hogy az aszinkron hívások bevárják egymást.
 */

import mysql from 'mysql';

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'webprog',
  password: 'VgJUjBd8',
  database: 'music',
});

function runQuery(query, options = []) {
  // Ez a rész kiváltható az util.promisigy vagy mysql2 függőség használatával
  return new Promise((resolve, reject) => {
    connection.query(query, options, (error, results) => {
      if (error) {
        reject(new Error(`Error while running '${query}: ${error}'`));
      }
      resolve(results);
    });
  });
}

const findAllTracksByAlbumId = (albumId) => runQuery('SELECT * FROM track where album_id=?', [albumId]);
const findAllAlbumsByArtistId = (artistId) => runQuery('SELECT * FROM album where artist_id=?', [artistId]);
const findAllArtists = () => runQuery('SELECT * FROM artist');

function printAlbumsToConsole(album) {
  console.log(`\tAlbum: ${album.name}`);
  album.tracks.forEach((track) => console.log(`\t\t${track.title}`));
}

function printArtistToConsole(artist) {
  console.log(`Artist: ${artist.name}`);
  artist.albums.forEach(printAlbumsToConsole);
  console.log('----------------------');
}

function loadTracks(album) {
  return findAllTracksByAlbumId(album.id).then((tracks) => ({
    ...album, // spread operátor: átveszi az objektum összes adattagját
    tracks,
  }));
}

function loadAlbums(artist) {
  return findAllAlbumsByArtistId(artist.id)
    .then((albums) => Promise.all(albums.map(loadTracks)).then((items) => ({
      ...artist, // spread operátor: átveszi az objektum összes adattagját
      albums: items,
    })));
}

function main() {
  findAllArtists().then((artists) => {
    Promise.all(artists.map(loadAlbums)).then((items) => {
      items.forEach(printArtistToConsole);
      connection.destroy();
    });
  });
}

main();
