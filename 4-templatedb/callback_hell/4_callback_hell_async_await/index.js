/**
 * Az alábbi példaprogram az 2_callback_hell_function_handler katalógusban levő megoldást
 * egészíti ki oly módon, hogy az aszinkron hívások bevárják egymást.
 */

import util from 'util';
import mysql from 'mysql';

const pool = mysql.createPool({
  host: 'localhost',
  user: 'webprog',
  password: 'VgJUjBd8',
  database: 'music',
});

// query promise-ifikált változata
const runQuery = util.promisify(pool.query).bind(pool);

const findAllTracksByAlbumId = async (albumId) => runQuery('SELECT * FROM track where album_id=?', [albumId]);
const findAllAlbumsByArtistId = (artistId) => runQuery('SELECT * FROM album where artist_id=?', [artistId]);
const findAllArtists = () => runQuery('SELECT * FROM artist');

async function loadTracks(album) {
  return {
    ...album,
    tracks: await findAllTracksByAlbumId(album.id),
  };
}

async function loadAlbums(artist) {
  return {
    ...artist,
    albums: await Promise.all(
      (await findAllAlbumsByArtistId(artist.id)).map(loadTracks),
    ),
  };
}

function printAlbumsToConsole(album) {
  console.log(`\tAlbum: ${album.name}`);
  album.tracks.forEach((track) => console.log(`\t\t${track.title}`));
}

function printArtistToConsole(artist) {
  console.log(`Artist: ${artist.name}`);
  artist.albums.forEach(printAlbumsToConsole);
  console.log('----------------------');
}

async function main() {
  const artists = await Promise.all((await findAllArtists())
    .map(loadAlbums));
  artists.forEach(printArtistToConsole);
  pool.end();
}

main();
