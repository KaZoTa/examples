/* eslint-disable */
/**
 * Az alábbi példaprogram a callback hell problémát igyekszik szemléltetni.
 * Ha lehet kerüljük a callback hell-t a házifeladatok elkészítésekor.
 * A bemutatott megoldás több szempontból is helytelen:
 * - az aszinkron hívások nem várják be egymást, a program futtatása eredményeképpen
 * először az előadók majd az albumok és ezt követően a zeneszámok listáját kapjuk meg.
 * Az elvárt viselkedés az lenne, hogy minden előadóhoz kiírjuk az albumok listáját,
 * majd minden albumhoz kiírjuk a rajta levő zeneszámokat;
 * - nehezen követhető/átlátható/módosítható a kód, mivel a komponensek erősen egymásba
 * kapcsolódnak;
 * - gyakran kód duplikáláshoz vezet, mivel az egyes hívások nem újrahasznosíthatóak
 *      => gyakran feleslegesen megnöveli a kód méretét;
 * - nagyon nehéz nyomkövetni;
 * - nagyon könnyű elrontani a zárójelezést;
 */
import mysql from 'mysql';

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'webprog',
  password: 'VgJUjBd8',
  database: 'music',
});

function main() {
  connection.query('SELECT * FROM artist', (error, artists) => {
    if (error) {
      console.error(`Unable to return the list of artists: ${error}`);
    }
    artists.forEach((artist) => {
      console.log(`Előadó: ${artist.name}`);
      connection.query('SELECT * FROM album where artist_id=?', [artist.id], (error2, albums) => {
        if (error2) {
          console.error(`Unable to return the list of albums: ${error2}`);
        }
        albums.forEach((album) => {
          console.log(`\tAlbum: ${album.name}`);
          connection.query('SELECT * FROM track where album_id=?', [album.id], (error3, tracks) => {
            if (error3) {
              console.error(`Unable to return the list of tracks: ${error3}`);
            }
            tracks.forEach((track) => {
              console.log(`\t\tTrack: ${track.title}`);
            });
          });
        });
      });
    });
  });
}

main();
