/**
 * Az alábbi példaprogram az 1_callback_hell_problem katalógusban levő megoldást teszi átláthatóbbá.
 * Ez a megoldás szintén helytelen a visszatérített értékek szempontjából,
 * viszont a kód jobban karbantartható és újrahasznosítható.
 */

import mysql from 'mysql';

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'webprog',
  password: 'VgJUjBd8',
  database: 'music',
});

function findAllTracksByAlbumId(albumId, callback) {
  connection.query('SELECT * FROM track where album_id=?', [albumId], (error, tracks) => {
    if (error) {
      console.error(`Unable to return the list of tracks: ${error}`);
    }
    callback(tracks);
  });
}

function findAllAlbumByArtistId(artistId, callback) {
  connection.query('SELECT * FROM album where artist_id=?', [artistId], (error, albums) => {
    if (error) {
      console.error(`Unable to return the list of albums: ${error}`);
    }
    callback(albums);
  });
}

function findAllArtists(callback) {
  connection.query('SELECT * FROM artist', (error, artists) => {
    if (error) {
      console.error(`Unable to return the list of artists: ${error}`);
    }
    callback(artists);
  });
}

function listTracksByAlbumId(albumId) {
  findAllTracksByAlbumId(albumId, (tracks) => {
    tracks.forEach((track) => {
      console.log(`\t\tTrack: ${track.title}`);
    });
  });
}

function listAlbumsByArtistId(artistId) {
  findAllAlbumByArtistId(artistId, (albums) => {
    albums.forEach((album) => {
      console.log(`\tAlbum: ${album.name}`);
      listTracksByAlbumId(album.id);
    });
  });
}

function listArtists() {
  findAllArtists((artists) => {
    artists.forEach((artist) => {
      console.log(`Előadó: ${artist.name}`);
      listAlbumsByArtistId(artist.id);
    });
  });
}

function main() {
  listArtists();
}

main();
