// Adatbázis műveleteket végző modul
import dbConnection from './connection.js';

/**
 * Aszinkron létrehozása a táblázatnak.
 * Async/await notációt használ.
 */
export const createTable = async () => {
  try {
    await dbConnection.executeQuery(`CREATE TABLE IF NOT EXISTS requests (
      method varchar(20),
      url varchar(50),
      date varchar(50));
    `);
    console.log('Table created successfully');
  } catch (err) {
    console.error(`Create table error: ${err}`);
    process.exit(1);
  }
};

// service metódus - lekéri az összes sort
// Promise-t térít vissza
export const findAllRequests = () => {
  const query = 'SELECT * FROM requests';
  return dbConnection.executeQuery(query);
};

// service metódus - beszúr egy DB sort
export const insertRequest = (req) => {
  const date = new Date().toISOString();
  const query = `INSERT INTO requests VALUES (
    "${req.method}", "${req.url}", "${date}");`;
  return dbConnection.executeQuery(query);
};

// service metódus - törli az összes sort
export const deleteAllRequests = () => {
  const query = 'DELETE FROM requests';
  return dbConnection.executeQuery(query);
};
