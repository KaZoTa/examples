import { insertRequest } from '../db/requests.js';

// loggoljunk minden kérést az adatbázisba, mint middleware
export default async function requestLogger(req, res, next) {
  try {
    // bevárjuk a beszúrási promise-t
    await insertRequest(req);
    // ha nincs hiba, a middleware továbbengedheti a hívást
    next();
  } catch (err) {
    res.status(500).render('error', { message: `Insertion unsuccessful: ${err.message}` });
  }
}
