/*
Példa egy egyszerű web-szerverre amely HTTP kéréseket fogad és kiírja a
console-ra a kérés metódusát, az URL-t, valamint a kérés fejlécét

EJS template engine-t használ
Figyelem!: noha nem hívunk "import"-ot rá, az "ejs" függőség szükséges
  az express app helyes beállításához.
*/

import express from 'express';
import path from 'path';

const app = express();

// beállítjuk az ejs engine-t
app.set('view engine', 'ejs');
// megadjuk, hogy hol találhatóak a sablon file-ok
// ilyen formában nem szükséges, mert a "views" az alapértelmezett érték
app.set('views', path.join(process.cwd(), 'views'));

app.use((request, response) => {
  // beállítjuk a view modelljét
  const model = {
    method: request.method,
    url: request.url,
    headers: request.headers,
  };

  // views/requestinfo.ejs sablonnal
  // felépítjük a választ a kliensnek
  response.render('requestinfo', model);
});

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
