/*
Csatlakozik egy MS SQL adatbázishoz egy connection segítségével.
Minden HTTP kérés érkezése esetén beírja az adatbázisba
a kérés metódusát, az URL-t és a kérés időpontját.

Figyelem! npm i csak az MSSQL-hez csatlakoztató segédmodult telepíti, nem magát a MSSQL-t.
*/
import express from 'express';
import sql from 'mssql';

const connectionConfig = {
  server: 'localhost',
  user: 'webprog',
  password: 'VgJUjBd8',
  database: 'webprog',
  options: {
    trustServerCertificate: true,
  },
};

// létesít egy kapcsolatot az adatbázissal
// a megadott DB-nek/felhasználónak léteznie kell
// létre lehet hozni a mellékelt setup.sql szkript segítségével
const pool = new sql.ConnectionPool(connectionConfig);
pool.connect((err) => {
  if (err) {
    console.error(`Connection error: ${err.message}`);
    process.exit(1);
  }

  const createQuery = `
  IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='requests' and xtype='U')
    CREATE TABLE requests (
      method varchar(20),
      url varchar(50),
      date varchar(50)
    )`;

  pool.query(createQuery, (createErr) => {
    if (createErr) {
      console.error(`Create table error: ${createErr.message}`);
      process.exit(1);
    } else {
      console.log('Table exists successfully');
    }
  });
});

const app = express();

app.use((req, res) => {
  const { method, url } = req;
  const date = new Date().toISOString();

  // felépítjük a végrehajtandó SQL lekérdezést
  const query = `INSERT INTO requests (method, url, date) VALUES ('${method}', '${url}', '${date}')`;
  console.log(`Executing query ${query}`);

  // végrehatjuk a lekérdezést
  pool.query(query, (err) => {
    if (err) {
      const msg = `Insertion unsuccessful: ${err.message}`;
      console.error(msg);
      res.status(500).send(msg);
    } else {
      res.send('Insertion successful');
    }
  });
});

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
