/*
Egyszerű példa az URL alapú routingra a http modullal.
A példa egy számot tart karban - lekérés, növelés és csökkentés lehetségesek.

Tesztkérések:
  - http://localhost:8080/query
  - http://localhost:8080/reset
  - http://localhost:8080/increment
  - http://localhost:8080/increment?with=10
  - http://localhost:8080/decrement
  - http://localhost:8080/decrement?with=10
*/

import { createServer } from 'http';
import { URL } from 'url';
import { parse } from 'querystring';

// számunk kiinduló értéke
let myNumber = 42;

const server = createServer((request, response) => {
  const body = [];

  request.on('data', (chunk) => body.push(chunk));
  request.on('error', (err) => console.error(err));

  request.on('end', () => {
    // lekérjük a pathname-et
    const parsedUrl = new URL(request.url, 'http://localhost:8080/');
    const query = parse(parsedUrl.search.substr(1));
    console.log(`Request with pathname ${parsedUrl.pathname} arrived`);

    // aszerint különböző műveleteket végzünk
    switch (parsedUrl.pathname) {
      case '/query':
        response.end(`The number is ${myNumber}`);
        break;
      case '/reset':
        myNumber = 42;
        response.end(`Reset, now the number is ${myNumber}`);
        break;
      case '/increment':
        myNumber += Number(query.with || 1);
        response.end(`Incremented, now the number is ${myNumber}`);
        break;
      case '/decrement':
        myNumber -= Number(query.with || 1);
        response.end(`Decremented, now the number is ${myNumber}`);
        break;
      default: // ismeretlen útvonal esetén hiba
        response.writeHead(404);
        response.end();
    }
  });
});

server.listen(8080, () => { console.log('Server listening...'); });

// Futtatás: node simplerouter.js
