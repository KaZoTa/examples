// Használjuk az fs (file system) modult
// egy állomány változásainak figyelésére,
// valamint tartalmának kiolvasására

// az fs (file sytem) beépített modul interakciót enged a fájlrendszerrel
// https://nodejs.org/api/fs.html
import fs from 'fs';

const fileName = 'valami.txt';

// a watch függvény figyeli a változásokat - 2. paramétere egy callback függvény
fs.watch(fileName, () => {
  console.log(`A ${fileName} állomány módosult!`);
  // a readFile olvassa az állomány tartalmát, majd meghívja a callbacket
  fs.readFile(fileName, (err, data) => {
    if (err) throw err;
    console.log(`Az állomány tartalma:\n${data}`);
  });
});

// egyből kiíródik ez az üzenet (mert a fenti függvények nem hívódnak meg csak események nyomán)
console.log(`Figyelem a ${fileName} állomány tartalmát`);

// futtatás: node watcher.js
// megj: a valami.txt állománynak léteznie kell!
