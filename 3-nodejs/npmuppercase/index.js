/*
Függőséggel ellátott projekt. Az alábbi import nem tud végbemenni,
ha nem töltjük be npm-mel a függőségeket.
A package.json definiálja ezeket, ezért csak "npm i"-t elegendő hívni.

Projekt inicializálása (felhasználóknak már nem kell meghívni):
  npm init
  npm i --save upper-case
  npm start

Későbbi futtatás (mondjuk git clone után):
  npm i
  npm start
*/

// külső függőség
import { upperCase } from 'upper-case';

console.log(upperCase('I am not screaming'));
