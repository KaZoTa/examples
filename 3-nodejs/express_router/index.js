import express from 'express';
import birds from './routes/birds.js';

const app = express();

/*
Bizonyos URL-re kötött router
A path-ek konkatenálódnak fastruktúra szerint
Hívások mehetnek a következőkre:
 - /birds/
 - /birds/about
*/
app.use('/birds', birds);

app.listen(8080);
