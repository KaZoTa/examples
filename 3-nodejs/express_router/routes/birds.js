import { Router } from 'express';

// felépítünk egy moduláris routert
const router = Router();

// csak ezen routerhez tartozó middleware
router.use((req, res, next) => {
  console.log(`${req.method} ${req.url}`);
  next();
});
// homepage
router.get('/', (req, res) => {
  res.send('Birds home page');
});
// about
router.get('/about', (req, res) => {
  res.send('About birds');
});

export default router;
