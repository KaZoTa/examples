/*
File-feltöltést engedélyez HTML formon keresztül.
A feltöltött állományokat elmenti egy almappába
*/

import express from 'express';
import { existsSync, mkdirSync } from 'fs';
import { join } from 'path';
import eformidable from 'express-formidable';

const app = express();
const uploadDir = join(process.cwd(), 'uploadDir');

// feltöltési mappa elkészítése
if (!existsSync(uploadDir)) {
  mkdirSync(uploadDir);
}

// a static mappából adjuk a HTML állományokat
app.use(express.static(join(process.cwd(), 'static')));

// formidable-lel dolgozzuk fel a kéréseket
app.use(eformidable({ uploadDir, keepExtensions: true }));

// formfeldolgozás
app.post('/upload_file', (request, response) => {
  // az állományok a request.files-ban lesznek
  const fileHandler = request.files.myfile;
  // más mezők a request.fields-ben
  const privateFile = Boolean(request.fields.private);

  const respBody = `Feltöltés érkezett:
    állománynév: ${fileHandler.name}
    név a szerveren: ${fileHandler.path}
    privát: ${privateFile}
  `;

  console.log(respBody);
  response.set('Content-Type', 'text/plain;charset=utf-8');
  response.end(respBody);
});

app.listen(8080, () => {
  console.log('Server listening...');
});
