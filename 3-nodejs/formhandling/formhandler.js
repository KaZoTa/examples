/*
Form adatok lekezelését végzi.
A statikus mappában adott egy HTML egy formmal - ezt szolgáljuk fel GET híváskor a gyökérre
Az ottani form leadását feldolgozzuk itt express segítségével.
*/

import express from 'express';
import { join } from 'path';

const app = express();

// a static mappából adjuk a HTML állományokat
app.use(express.static(join(process.cwd(), 'static')));

// standard kérésfeldolgozással kapjuk a body tartalmát
app.use(express.urlencoded({ extended: true }));

// formfeldolgozás
app.post('/submit_form', (request, response) => {
  const birthDate = new Date(request.body.birthdate);
  const knowsHtml = Boolean(request.body.knowshtml);

  const respBody = `A szerver sikeresen megkapta a következő információt:
    név: ${request.body.name}
    jelszó: ${request.body.password} (sosem szabad ezt így kiírni :))
    születési dátum: ${birthDate}
    nem: ${request.body.gender}
    tud HTML-t: ${knowsHtml}
  `;
  console.log(respBody);

  response.set('Content-Type', 'text/plain;charset=utf-8');
  response.end(respBody);
});

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
