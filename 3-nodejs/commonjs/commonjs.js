// külső modulok betöltése

// külső (node-hoz tartozó) modul betöltése
const fs = require('fs');

// belső (relatív útvonalon elhelyezkedő) modul betöltése
const lib = require('./commonjs_lib.js');

console.log(fs.readdirSync(process.cwd()));

lib.libFunction1();
lib.libFunction2();

// Futtatás: node commonjs_main.js
