function libFunction1() {
  console.log('I am libfunction1');
}

function libFunction2() {
  console.log('I am libfunction2');
}

// eslint-disable-next-line
function privateFunction() {
  console.log('I am a non-exported function');
}

// ha külső pont importálja (require) ezt a modult,
// az alább beállított objektumot kapja vissza
module.exports = {
  libFunction1,
  libFunction2,
};
