// Példa egy egyszerű web-szerverre amely GET kéréseket fogad és kiírja a
// console-ra a kérés metódusát, az URL-t valamint a kérés fejlécét

import express from 'express';

// Elkészítjük az express alkalmazást
const app = express();

// HTTP GET kérésre való reakció
app.get('/*', (request, response) => {
  const info = `You have requested:
    - method: ${request.method}
    - URL: ${request.url}
    - headers (as JSON): ${JSON.stringify(request.headers)}
  `;
  console.log(info);
  response.send(info);
});

// A listen metódus hívásának hatására aktiválódik a szerver
// és fogadja a 8080-as porton beérkező kéréseket
app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });

// Futtatás: node simpleserver.js
// Előtte telepíteni az express modult!!!
//    npm install express
