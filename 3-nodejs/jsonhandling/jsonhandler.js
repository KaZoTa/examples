/* JSON adatok kezelése */

import express from 'express';
import bodyParser from 'body-parser';

const app = express();

// JSON formátumú body feldolgozása
app.use(bodyParser.json());

// JSON formátumú információ feldolgozása
app.post('/submit_json', (request, response) => {
  console.log('A szerver sikeresen megkapta a következő információt JSON formátumban');
  console.log(request.body);

  // felépítünk egy komplex választ
  // az express automatikusan JSON-ban küldi vissza
  response.send({
    reqBody: request.body,
    date: new Date(),
  });
});

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
