// Egyszerű példa egy express middleware-re, amely
// kinaplóz minden hívást, mielőtt továbbengedi egy statikus feldolgozónak

import express from 'express';
import { join } from 'path';

const staticDir = join(process.cwd(), 'static');

const app = express();

// loggoló middleware - minden híváskor kikötünk itt
app.use((req, resp, next) => {
  const date = new Date();
  console.log(`${date} ${req.method} ${req.url}`);
  // továbbengedjük a hívási láncot
  next();
});

app.use(express.static(staticDir));

app.listen(8080, () => { console.log('Server listening on http://localhost:8080/ ...'); });
