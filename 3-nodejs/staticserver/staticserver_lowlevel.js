/*
Egyszerű példa egy http szerverre amely statikus állományokat küld vissza válaszban
a kliensnek. Amennyiben a kért állomány nem található, 404-es hibakóddal válaszol

Tesztelni lehet:
- böngészőből: http://localhost:8080/allomanynev
- Postmannel, curl-lel
*/

import http from 'http';
import fs from 'fs';
import path from 'path';
import mimeTypes from 'mime-types';

// a mappa ahonnan statikus tartalmat szolgálunk
// SOSE a gyökeret tegyünk publikussá
const staticDir = path.join(process.cwd(), 'static');

const indexFile = 'index.html';

const server = http.createServer((request, response) => {
  request.on('data', () => {});
  request.on('error', (err) => console.error(err));
  request.on('end', () => {
    console.log(`Received request for ${request.url}`);
    let filename = path.join(staticDir, request.url); // Sebezhető. Miért?

    fs.stat(filename, (err, stats) => {
      // hiba = az állomány nem létezik
      if (err) {
        console.error(`  ${filename} does not exist`);
        response.statusCode = 404;
        response.end();
        return;
      }

      if (stats.isDirectory()) {
        filename = path.join(filename, indexFile);
      }

      // az állomány nevéből megpróbálunk Content-Type-ot deriválni
      const mimeType = mimeTypes.lookup(filename) || 'text/plain';
      response.setHeader('Content-Type', mimeType);
      console.log(`  ${filename} exists, content-type=${mimeType}`);

      // beolvassuk az állományt, átírjuk a response-ba
      const readStream = fs.createReadStream(filename);
      readStream.pipe(response);
      // A readStream.pipe lenyegeben a kovetkezo sorokat helyettesiti:
      // readStream.on('data', data => response.write(data));
      // readStream.on('end', () => response.end());
    });
  });
});

server.listen(8080, () => { console.log('Server listening...'); });

/*
Futtatás:
  npm install mime-types
  node staticserver_lowlevel.js
*/
