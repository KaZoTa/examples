// külső modulok betöltése

// külső (node-hoz tartozó) modul betöltése
import fs from 'fs';

// belső (relatív útvonalon elhelyezkedő) modul betöltése
import libFunction from './es2015module_default.js';

import { libFunction1, libFunction2 } from './es2015module_multi.js';

// az összes exportált függvény közös néven való betöltése
// import * as libMulti from './es2015module_multi.js';

// függvények használata

console.log('Contents of current directory', fs.readdirSync('.'));

libFunction();
libFunction1();
libFunction2();

// Futtatás: node index.js
