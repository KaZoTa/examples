import { MongoClient, ObjectId } from 'mongodb';

// alább aszinkron módon felépített adatbázis-kapcsolat
let collection;

export default async function connectToMongo() {
  const url = 'mongodb://127.0.0.1:27017';
  console.log(`Connecting to Mongo at ${url}`);
  const client = new MongoClient(url);
  await client.connect();
  console.log('Connected successfully to server');
  const db = client.db('quizzes');
  collection = db.collection('quizzes');
}

export function findAllQuizes() {
  return collection.find({}).toArray();
}

export function findQuizById(quizID) {
  return collection.findOne({ _id: new ObjectId(quizID) });
}

export function insertQuiz(quiz) {
  return collection.insertOne(quiz);
}

export async function deleteQuiz(quizID) {
  const result = await collection.deleteOne({ _id: new ObjectId(quizID) });
  return result.deletedCount > 0;
}
