import express from 'express';
import morgan from 'morgan';
import cors from 'cors';

import quizRouter from './router/quiz.js';
import connectToMongo from './db/quiz.js';

connectToMongo().then(() => {
  const app = express();

  app.use(cors({ origin: 'http://localhost:3000' }));
  app.use(morgan('tiny'));
  app.use(express.json());
  app.use('/api/quizes', quizRouter);

  app.listen(8080, () => {
    console.log('Server listening on http://localhost:8080/');
  });
});
