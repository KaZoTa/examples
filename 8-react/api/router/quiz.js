import express from 'express';
import * as db from '../db/quiz.js';

const router = express.Router();

router.get('/', async (req, res) => {
  const quizes = await db.findAllQuizes();
  res.send(quizes);
});

router.get('/:quizID', async (req, res) => {
  const { quizID } = req.params;
  const quiz = await db.findQuizById(quizID);
  if (quiz) {
    res.send(quiz);
  } else {
    res.sendStatus(404);
  }
});

router.post('/', async (req, res) => {
  await db.insertQuiz(req.body);
  res.sendStatus(204);
});

router.delete('/:quizID', async (req, res) => {
  const { quizID } = req.params;
  await db.deleteQuiz(quizID);
  res.sendStatus(204);
});

export default router;
