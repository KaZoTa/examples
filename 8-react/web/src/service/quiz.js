import { apiBase } from '../util/constants';

export async function fetchAllQuizzes() {
  const response = await fetch(`${apiBase}/quizes`);
  const quizzes = await response.json();
  return quizzes;
}

export async function fetchQuizById(quizId) {
  const response = await fetch(`${apiBase}/quizes/${quizId}`);
  const quiz = await response.json();
  return quiz;
}

export async function createQuiz(quiz) {
  await fetch(`${apiBase}/quizes`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(quiz),
  });
}

export async function deleteQuizById(quizId) {
  await fetch(`${apiBase}/quizes/${quizId}`, {
    method: 'DELETE',
  });
}
