import { Link } from 'react-router-dom';

export default function NavBar() {
  return (
    <nav>
      <Link className="menu-button" to="/">Quizes</Link>
      <Link className="menu-button" to="/create_quiz">Create Quiz</Link>
    </nav>
  );
}
