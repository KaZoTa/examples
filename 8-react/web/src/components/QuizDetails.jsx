import { useState, useEffect } from 'react';
import { fetchQuizById } from '../service/quiz';
import { Link, useParams } from 'react-router-dom';

export default function QuizDetails() {
  const [quiz, setQuiz] = useState(null);

  // útvonal paraméter
  const { quizId } = useParams();

  // quizek betöltése első mountkor
  useEffect(() => {
    fetchQuizById(quizId).then(setQuiz);
  }, [quizId]);

  // ha még nem töltődött be a quiz, loading üzenet
  if (!quiz) {
    return <i>Loading quiz...</i>;
  }

  return (
    <>
      <h1>{quiz.name}</h1>
      <div className="quiz">
        <div className="date">{quiz.date}</div>
        <div>{quiz.points} pts</div>
        <div className="description">{quiz.description}</div>
        <Link to="/">Return to main page</Link>
      </div>
    </>
  );
}
