import { useState } from 'react';
import { Link } from 'react-router-dom';
import { deleteQuizById } from '../service/quiz';

export default function QuizListEntry({ quiz }) {
  const [deleted, setDeleted] = useState(false);

  const onDeleteQuiz = () => {
    // eslint-disable-next-line no-restricted-globals
    if (confirm(`Are you sure you want to delete quiz ${quiz.name}?`)) {
      deleteQuizById(quiz._id)
        .then(() => setDeleted(true));
    }
  }

  if (deleted) {
    return <></>;
  }

  return (
    <div className="quiz">
      <h2>
        <Link to={`/quizzes/${quiz._id}`}>{quiz.name}</Link>
        <input className="menu-button" type="button" value="Delete" onClick={onDeleteQuiz} />
      </h2>
      <div className="date">{quiz.date}</div>
      <div>{quiz.points} pts</div>
    </div>
  );
}
