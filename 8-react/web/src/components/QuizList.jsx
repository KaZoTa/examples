import { useState, useEffect } from 'react';
import QuizListEntry from './QuizListEntry';
import { fetchAllQuizzes } from '../service/quiz';

export default function QuizList() {
  const [quizzes, setQuizzes] = useState([]);

  // quizek betöltése első mountkor
  useEffect(() => {
    fetchAllQuizzes().then(setQuizzes);
  }, []);

  return (
    <>
      <h1>Quizes</h1>
      <div id="container">
        {quizzes.map((quiz) => (
          <QuizListEntry key={quiz._id} quiz={quiz} />
        ))}
      </div>
    </>
  );
}
