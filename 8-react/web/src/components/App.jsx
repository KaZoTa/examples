import { BrowserRouter, Routes, Route } from 'react-router-dom';

import NavBar from './NavBar';
import QuizList from './QuizList';
import Footer from './Footer';
import QuizCreationForm from './QuizCreationForm';
import QuizDetails from './QuizDetails';

export default function App() {
  return (
    <BrowserRouter>

      {/* navbar minden oldal tetején */}
      <NavBar />

      {/* főoldal route-ok szerint a 2 között */}
      <main>
        <Routes>
          <Route path="/" element={<QuizList />} />
          <Route path="/create_quiz" element={<QuizCreationForm />} />
          <Route path="/quizzes/:quizId" element={<QuizDetails />} />
          <Route path="*" element={<i>Cannot find page</i>} />
        </Routes>
      </main>

      {/* lábléc minden oldal alján */}
      <Footer />

    </BrowserRouter>
  );
}