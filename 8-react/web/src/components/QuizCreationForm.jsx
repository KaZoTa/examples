import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { createQuiz } from '../service/quiz';

export default function QuizCreationForm() {
  const [name, setName] = useState('');
  const [date, setDate] = useState(new Date());
  const [points, setPoints] = useState(42);
  const [description, setDescription] = useState('');

  const nav = useNavigate();

  const submitForm = () => {
    const quiz = { name, date, points, description };
    // létrehozzuk a kvízt, siker esetén átirányítunk a főoldalra
    createQuiz(quiz).then(() => nav('/'))
  };

  return (
    <>
      <h1>Create Quiz</h1>

      <label htmlFor="name">Quiz Name: </label>
      <input id="name" type="text" name="name" placeholder="Quiz Name" value={name} onChange={(e) => setName(e.target.value)} />
      <br />

      <label htmlFor="date"> Date: </label>
      <input id="date" type="datetime-local" name="date" value={date} onChange={(e) => setDate(e.target.value)} />
      <br />

      <label htmlFor="point">Points: </label>
      <input id="point" type="number" name="point" value={points} onChange={(e) => setPoints(e.target.value)} />
      <br />

      <label htmlFor="description">Description: </label>
      <textarea name="description" id="description" cols="30" rows="10" placeholder="Description..." value={description} onChange={(e) => setDescription(e.target.value)}></textarea>
      <br />

      <input className="menu-button" type="button" value="Submit" onClick={submitForm} />
    </>
  );
}
