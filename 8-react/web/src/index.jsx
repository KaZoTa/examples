import { createRoot } from 'react-dom/client';
import App from './components/App';

import './style.css';

const root = document.getElementById('root');
createRoot(root).render(<App />);
