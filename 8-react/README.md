# React példa

A projekt az alábbi 3 részből áll.

## 1. API szerver

- express + MongoDB API szerver, quizeket tart karban
- nem használ sablonmotrot, mindig JSON-t térít vissza
- aszinkron hívásokkal kell elérni
- a `cors` modul segítségével elérhető aszinkron hívásokkal más útvonalról is, mint ahova a szerver ki van telepítve (ebben az esetben a React kliensről)
- használat:
```bash
    cd api
    npm install
    npm start
```
- endpoints:
    - `GET http://localhost:8080/api/quizes` - list of quizes
    - `GET http://localhost:8080/api/quizes/1` - quiz with given ID with description, 404 if missing
    - `POST http://localhost:8080/api/quizes` - create quiz based on JSON body
    - `DELETE http://localhost:8080/api/quizes/1` - delete quiz with given ID

## 2. Statikus kliensoldal

- HTML oldalak, amelyek kiindulópontot adnak a frontendhez
- megtekinthető `serve`-vel, nem kommunikál a szerverrel még
- használat:
```bash
    cd staticweb
    serve
```

## 3. React kliensoldal

- a statikus HTML-ek alapján kialakított React kliensoldal
- JSX kód, amely aszinkron hívásokkal kommunikál az API szerverrel
- használat:
```bash
    cd web
    npm install
    npx react-scripts start
```
